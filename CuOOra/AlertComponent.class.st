"
Mensaje de alerta.
"
Class {
	#name : #AlertComponent,
	#superclass : #WAComponent,
	#instVars : [
		'mensaje'
	],
	#category : #'CuOOra-ui'
}

{ #category : #callbacks }
AlertComponent >> acept [
	self session unregister.
	self requestContext redirectTo: self application url
]

{ #category : #accessing }
AlertComponent >> mensaje [
	^ mensaje
]

{ #category : #accessing }
AlertComponent >> mensaje: anObject [
	mensaje := anObject
]

{ #category : #rendering }
AlertComponent >> renderContentOn: aCanvas [
	aCanvas paragraph: self mensaje.
	aCanvas anchor
		callback: [ self acept ];
		with: [aCanvas button:'aceptar'].
]
