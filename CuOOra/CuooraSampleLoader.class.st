"
Loader que carga todos los metodos de ejemplo para la entrega.
"
Class {
	#name : #CuooraSampleLoader,
	#superclass : #Object,
	#category : #'CuOOra-samples'
}

{ #category : #loader }
CuooraSampleLoader >> loadExampleIn: aCuoora [
	| pedro diego juan pregunta1 pregunta2 topico1 topico2 topico3 favs1 favs2 respuesta1 respuesta2 |
	pedro := Usuario
		nombre: 'pedro@cuoora.com'
		contraseña: 'pedro@cuoora.com'.
	aCuoora crearUsuario: pedro.
	diego := Usuario
		nombre: 'diego@cuoora.com'
		contraseña: 'diego@cuoora.com'.
	aCuoora crearUsuario: diego.
	juan := Usuario
		nombre: 'juan@cuoora.com'
		contraseña: 'juan@cuoora.com'.
	aCuoora crearUsuario: juan.
	juan seguirA: diego.
	pedro seguirA: juan.
	topico1 := Topico nombre: 'OO1' descripcion: 'la materia'.
	aCuoora agregarTopico: topico1.
	topico2 := Topico nombre: 'Test de unidad' descripcion: 'un test'.
	aCuoora agregarTopico: topico2.
	topico3 := Topico nombre: 'Smalltalk' descripcion: 'el lenguaje'.
	aCuoora agregarTopico: topico3.
	favs1 := OrderedCollection new.
	favs1 add: topico1.
	favs1 add: topico2.
	favs2 := OrderedCollection new.
	favs2 add: topico1.
	favs2 add: topico3.
	pregunta1 := Pregunta
		cuerpo: 'Hola quisiera saber para que sirve este método'
		usuario: pedro
		titulo: '¿Para qué sirve el método SetUp?'
		topicos: favs1.
	pedro agregarPregunta: pregunta1.
	respuesta1 := Respuesta
		cuerpo:
			'Sirve para instanciar los objetos que
son evaluados por el test en un único método y que se
ejecute siempre antes de cada test.'
		usuario: diego
		pregunta: pregunta1.
	pregunta1 agregarRespuesta: respuesta1.
	pregunta2 := Pregunta
		cuerpo: 'Hola quisiera saber que significa ese mensaje'
		usuario: diego
		titulo: '¿Qué significa #messageNotUnderstood?'
		topicos: favs2.
	diego agregarPregunta: pregunta2.
	respuesta2 := Respuesta
		cuerpo:
			'Significa que el objeto que recibió el
mensaje no encontró ningún método para ejecutar en
respuesta.'
		usuario: pedro
		pregunta: pregunta2.
	pregunta2 agregarRespuesta: respuesta2
]
