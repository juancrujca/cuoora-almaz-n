"
Una vez que eln usuario ingresa haciendo el login, el home del sitio le muestra el listado de las últimas 5 preguntas relevantes ordenadas según su fecha de creación. Por cada pregunta, se muestra su autor, cuántas respuestas tiene, y la cantidad de likes y dislikes. Debe ser posible desde esta vista votar las preguntas listadas y también crear una pregunta nueva.
Cuando el usuario hace click en alguna de las preguntas, la aplicación debe mostrar el detalle de la misma.
"
Class {
	#name : #HomeComponent,
	#superclass : #WAComponent,
	#instVars : [
		'relevantes'
	],
	#category : #'CuOOra-ui'
}

{ #category : #initialization }
HomeComponent >> initialize [
	super initialize.
	relevantes := self session user obtenerPreguntasRelevantes.
	relevantes sort: [ :a :b | a creacion > b creacion ]
]

{ #category : #accessing }
HomeComponent >> relevantes [
	^ relevantes 
]

{ #category : #rendering }
HomeComponent >> renderContentOn: aCanvas [
	self renderHeader: aCanvas.
	self renderRelevantes: aCanvas
]

{ #category : #rendering }
HomeComponent >> renderHeader: aCanvas [
	aCanvas
		paragraph: [ aCanvas div
				style:
					'display:flex; justify-content:space-between; align-items:center; background-color:orange';
				with: [ aCanvas heading: 'CuOOra'.
					aCanvas
						div: [ aCanvas anchor
							style:'text-decoration:none;color:black';
							callback:[self verPerfil:self session user];
							with:[aCanvas strong: self session user nombre].							
							aCanvas space.
							aCanvas space.
							aCanvas anchor
								callback: [ self unlog ];
								with: 'Log out' ] ] ].
	aCanvas horizontalRule.
	aCanvas
		paragraph: [ aCanvas div
				style: 'display:flex; justify-content:space-between; align-items:center';
				with: [ aCanvas anchor
						callback: [ self answer ];
						with: [ aCanvas button: '←' ].
					aCanvas anchor
						callback: [ self
								call:
									(PreguntarComponent
										new) ];
						with: [ aCanvas button: '+ Hacer nueva Pregunta' ] ] ]
]

{ #category : #rendering }
HomeComponent >> renderLikes: aCanvas relevante: r [
	aCanvas div
		style: 'overflow:hidden';
		with: [ aCanvas paragraph
				style: 'float:left; margin-left:5px';
				with: r likes size.
			aCanvas paragraph
				style: 'float:left; margin-left:5px';
				with: 'likes |'.
			aCanvas paragraph
				style: 'float:left; margin-left:5px';
				with: r dislikes size.
			aCanvas paragraph
				style: 'float:left; margin-left:5px';
				with: 'dislikes' ]
]

{ #category : #rendering }
HomeComponent >> renderRelevantes: aCanvas [
	aCanvas heading: 'Ultimas preguntas'.
	aCanvas
		unorderedList: [ relevantes
				do: [ :r | 
					self renderTitulo: aCanvas relevante: r.
					self renderTopicos: aCanvas relevante: r.
					aCanvas space.
					self renderUsuarioRespuesta: aCanvas relevante: r.
					self renderLikes: aCanvas relevante: r.
					self renderVotar: aCanvas relevante: r.
					aCanvas space.
					aCanvas horizontalRule ] ]
]

{ #category : #rendering }
HomeComponent >> renderTitulo:aCanvas relevante: r [
aCanvas heading
						level: 2;
						with: [ aCanvas anchor
								style: 'color:black';
								callback: [ self verPregunta: r ];
								with: r titulo ].
]

{ #category : #rendering }
HomeComponent >> renderTopicos: aCanvas relevante: r [
	aCanvas
		paragraph: [ r topicos
				do: [ :t | 
					aCanvas anchor
						style: 'color:grey;float:left; margin-left:5px';
						with: '#hola'.
					aCanvas anchor
						style: 'color:gray; float:left; margin-right:5px';
						with: t nombre ] ]
]

{ #category : #rendering }
HomeComponent >> renderUsuarioRespuesta: aCanvas relevante: r [
	aCanvas div
		style: 'overflow:hidden';
		with: [ aCanvas paragraph
				style: 'float:left; margin-left:5px';
				with: 'Realizada por '.
			aCanvas anchor
				style: 'float:left; margin-left:5px';
				callback: [ self verPerfil: r usuario ];
				with: [ aCanvas paragraph: r usuario nombre ].
			aCanvas paragraph
				style: 'float:left; margin-left:5px';
				with: '|'.
			aCanvas paragraph
				style: 'float:left; margin-left:5px';
				with: r obtenerRespuestasPregunta size.
			aCanvas paragraph
				style: 'float:left; margin-left:5px';
				with: 'respuestas' ]
]

{ #category : #rendering }
HomeComponent >> renderVotar: aCanvas relevante:r [ 
aCanvas div
						style: 'overflow hidden';
						with: [ aCanvas anchor
								style: 'float:left; margin-left:5px';
								callback: [ r darLike: self session user ];
								with: 'like'.
							aCanvas anchor
								style: 'color:blue;float:left; margin-left:5px';
								with: '|'.
							aCanvas anchor
								style: 'float:left; margin-left:5px';
								callback: [ r darDislike: self session user ];
								with: 'dislike' ].
]

{ #category : #callbacks }
HomeComponent >> unlog [
self session unregister.
self requestContext redirectTo: self application url. 
]

{ #category : #callbacks }
HomeComponent >> verPerfil: unUsuario [
	self
		call:
			(PerfilComponent
				new usuario: unUsuario ;
				yourself)
]

{ #category : #callbacks }
HomeComponent >> verPregunta: unaPregunta [
	self
		call:
			(PreguntaComponent
				new pregunta: unaPregunta ;
				yourself)
]
