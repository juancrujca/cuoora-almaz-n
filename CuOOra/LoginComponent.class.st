"
Login de cuoora.

La expresión que hay que evaluar en el playground para comenzar a usar la página es :
| application |
application := WAAdmin register: LoginComponent asApplicationAt: 'cuooralmazán'.
application sessionClass: SessionWithUser.
Sistema clearSoleInstance .
CuooraSampleLoader new loadExampleIn: Sistema soleInstance.
"
Class {
	#name : #LoginComponent,
	#superclass : #WAComponent,
	#instVars : [
		'sistema',
		'usuario',
		'nombre',
		'contraseña'
	],
	#category : #'CuOOra-ui'
}

{ #category : #accessing }
LoginComponent >> contraseña [
	^ contraseña
]

{ #category : #accessing }
LoginComponent >> contraseña: anObject [
	contraseña := anObject
]

{ #category : #callbacks }
LoginComponent >> iniciarSesion [
	(sistema userWithNombre: nombre protectedBy: contraseña)
		ifNotNil: [ :it | self proceedWith: it ]
		ifNil: [ nombre := nil.
			contraseña := nil.
			self
				call: (AlertComponent new mensaje: 'Nombre y contraseña incorrectos';yourself) ]
]

{ #category : #initialization }
LoginComponent >> initialize [
	super initialize.
	sistema := Sistema soleInstance.

]

{ #category : #accessing }
LoginComponent >> nombre [
	^ nombre
]

{ #category : #accessing }
LoginComponent >> nombre: anObject [
	nombre := anObject
]

{ #category : #private }
LoginComponent >> proceedWith: unUsuario [ 
	self session user:unUsuario .
	self call: HomeComponent new
]

{ #category : #callbacks }
LoginComponent >> registrarse [
	| unUsuario |
	(sistema usuarios anySatisfy: [:u | u nombre = self nombre])
		ifTrue: [ nombre := nil.
			contraseña := nil.
			self
				call:
					(AlertComponent new
						mensaje: 'Este usuario ya existe';
						yourself) ]
		ifFalse: [ unUsuario := Usuario nombre: nombre contraseña: contraseña.
			sistema crearUsuario: unUsuario.
			self proceedWith: unUsuario ]
]

{ #category : #rendering }
LoginComponent >> renderContentOn: aCanvas [
	self renderHeader: aCanvas.
	self renderLogin: aCanvas.
]

{ #category : #rendering }
LoginComponent >> renderHeader: aCanvas [
	aCanvas
		paragraph: [ aCanvas div
				style:
					'display:flex; justify-content:space-between; align-items:center; background-color:orange';
				with: [ aCanvas heading: 'CuOOra'. ] ].
	aCanvas horizontalRule
]

{ #category : #rendering }
LoginComponent >> renderLogin: aCanvas [
	aCanvas form: [ aCanvas
				paragraph: [ aCanvas label: 'Usuario: '.
					aCanvas textInput on: #nombre of: self ].
			aCanvas
				paragraph: [ aCanvas label: 'Contraseña: '.
					aCanvas passwordInput on: #contraseña of: self ].
			aCanvas button
				callback: [ self iniciarSesion ];
				with: 'Iniciar Sesión'.
			aCanvas button
				callback: [ self registrarse ];
				with: 'Registrarse' ]
]

{ #category : #accessing }
LoginComponent >> usuario [
	^ usuario
]

{ #category : #accessing }
LoginComponent >> usuario: anObject [
	usuario := anObject
]
