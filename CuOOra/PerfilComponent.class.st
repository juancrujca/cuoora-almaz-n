"
Cada vez que aparece el nombre de un usuario en pantalla, este debe ser un link que permite navegar al perfil de dicho usuario, donde se muestran solamente las preguntas realizadas por él, ver Figura 4. Notar que en esta pantalla, el usuario que está navegando puede empezar a seguir (follow) al usuario cuyo perfil se está mostrando.
"
Class {
	#name : #PerfilComponent,
	#superclass : #WAComponent,
	#instVars : [
		'usuario'
	],
	#category : #'CuOOra-ui'
}

{ #category : #callbacks }
PerfilComponent >> follow [
	self session user seguirA: usuario.
]

{ #category : #rendering }
PerfilComponent >> renderContentOn: aCanvas [
	self renderHeader: aCanvas.
	self renderTitulo: aCanvas .
	self renderPreguntas: aCanvas
]

{ #category : #rendering }
PerfilComponent >> renderFollow: aCanvas [
	(self session user seguidos includes: usuario)
		ifFalse: [ aCanvas anchor
				style: 'float:left; margin-left:5px';
				callback: [ self follow ];
				with: [ aCanvas heading: 'follow' ] ]
		ifTrue: [ aCanvas heading
				style: 'float:left; margin-left:5px; color:grey';
				with: 'seguido' ]
]

{ #category : #rendering }
PerfilComponent >> renderHeader: aCanvas [
	aCanvas
		paragraph: [ aCanvas div
				style:
					'display:flex; justify-content:space-between; align-items:center; background-color:orange';
				with: [ aCanvas anchor
					      style:'color:black;text-decoration:none';
							callback:[self call: (HomeComponent new)];
					      with: [aCanvas heading: 'CuOOra'].
					aCanvas
						div: [ aCanvas strong: self session user nombre.
							aCanvas space.
							aCanvas space.
							aCanvas anchor
								callback: [ self unlog ];
								with: 'Log out' ] ] ].
	aCanvas horizontalRule.
	aCanvas anchor 
	callback: [ self answer ];
	with: [ aCanvas button: '<--' ]
]

{ #category : #rendering }
PerfilComponent >> renderPreguntas: aCanvas [
	aCanvas
		unorderedList: [ usuario obtenerPreguntasUsuario
				do: [ :p | 
					aCanvas heading
						level: 2;
						with: [ aCanvas anchor
								style: 'color:black';
								callback: [ self verPregunta: p ];
								with: p titulo ].
					aCanvas
						paragraph: [ p topicos
								do: [ :t | 
									aCanvas anchor
										style: 'color:grey;float:left; margin-left:5px';
										with: '#'.
									aCanvas anchor
										style: 'color:gray; float:left; margin-right:5px';
										with: t nombre ] ].
					aCanvas space.
					aCanvas div
						style: 'overflow:hidden';
						with: [ aCanvas paragraph
								style: 'float:left; margin-left:5px';
								with: p obtenerRespuestasPregunta size.
							aCanvas paragraph
								style: 'float:left; margin-left:5px';
								with: 'respuestas' ].
					aCanvas div
						style: 'overflow:hidden';
						with: [ aCanvas paragraph
								style: 'float:left; margin-left:5px';
								with: p likes size.
							aCanvas paragraph
								style: 'float:left; margin-left:5px';
								with: 'likes |'.
							aCanvas paragraph
								style: 'float:left; margin-left:5px';
								with: p dislikes size.
							aCanvas paragraph
								style: 'float:left; margin-left:5px';
								with: 'dislikes' ].
					aCanvas div
						style: 'overflow hidden';
						with: [ aCanvas anchor
								style: 'float:left; margin-left:5px';
								callback: [ p darLike: self session user ];
								with: 'like'.
							aCanvas anchor
								style: 'color:blue;float:left; margin-left:5px';
								with: '|'.
							aCanvas anchor
								style: 'float:left; margin-left:5px';
								callback: [ p darDislike: self session user ];
								with: 'dislike' ].
					aCanvas space.
					aCanvas horizontalRule ] ]
]

{ #category : #rendering }
PerfilComponent >> renderTitulo: aCanvas [
	aCanvas div
		style: 'overflow:hidden';
		with: [ aCanvas heading
				style: 'float:left';
				with: 'Preguntas de'.
			aCanvas heading
				style: 'float:left; margin-left:5px';
				with: usuario nombre.
			self renderFollow: aCanvas  ]
]

{ #category : #callbacks }
PerfilComponent >> unlog [
	self session unregister.
	self requestContext redirectTo: self application url
]

{ #category : #accessing }
PerfilComponent >> usuario [
	^ usuario
]

{ #category : #accessing }
PerfilComponent >> usuario: anObject [
	usuario := anObject
]

{ #category : #callbacks }
PerfilComponent >> verPregunta: unaPregunta [
	self
		call:
			(PreguntaComponent new
				pregunta: unaPregunta;
				yourself)
]
