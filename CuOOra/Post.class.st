"
Un Post
"
Class {
	#name : #Post,
	#superclass : #Object,
	#instVars : [
		'creacion',
		'cuerpo',
		'usuario',
		'likes',
		'dislikes'
	],
	#category : #'CuOOra-model'
}

{ #category : #constructor }
Post class >> cuerpo: unCuerpo usuario: unUsuario [
	^ self new
		inicializarCuerpo: unCuerpo
		usuario: unUsuario
]

{ #category : #accessing }
Post >> buscarDislike: unVotante [
	^ dislikes anySatisfy: [ :dislike | dislike compararVotante: unVotante ]
]

{ #category : #accessing }
Post >> buscarLike: unVotante [
	^ likes anySatisfy: [ :like | like compararVotante: unVotante ]
]

{ #category : #methods }
Post >> calcularPuntajePost [
	^ (likes size - dislikes size)
]

{ #category : #accessing }
Post >> creacion [
	^ creacion
]

{ #category : #accessing }
Post >> cuerpo [
	^ cuerpo
]

{ #category : #methods }
Post >> darDislike: unVotante [
	(self buscarDislike: unVotante)
		ifFalse: [ dislikes add: (Votacion votante: unVotante) ].
	(self buscarLike: unVotante)
		ifTrue: [ likes
				remove: (likes detect: [ :like | like compararVotante: unVotante ]) ]
]

{ #category : #methods }
Post >> darLike: unVotante [
	(self buscarLike: unVotante)
		ifFalse: [ likes add: (Votacion votante: unVotante) ].
	(self buscarDislike: unVotante)
		ifTrue: [ dislikes
				remove: (dislikes detect: [ :dislike | dislike compararVotante: unVotante ]) ]
]

{ #category : #accessing }
Post >> dislikes [
	^ dislikes
]

{ #category : #initialization }
Post >> inicializarCuerpo: unCuerpo usuario: unUsuario [
	cuerpo := unCuerpo.
	usuario := unUsuario
]

{ #category : #initialization }
Post >> initialize [
	super initialize.
	creacion := DateAndTime now.
	usuario := Usuario new.
	likes := OrderedCollection new.
	dislikes := OrderedCollection new
]

{ #category : #accessing }
Post >> likes [
	^ likes
]

{ #category : #accessing }
Post >> usuario [
	^ usuario
]
