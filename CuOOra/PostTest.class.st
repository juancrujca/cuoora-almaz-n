Class {
	#name : #PostTest,
	#superclass : #TestCase,
	#instVars : [
		'post',
		'usuario1',
		'usuario2'
	],
	#category : #'CuOOra-tests'
}

{ #category : #setup }
PostTest >> setUp [
	super setUp.
	usuario1 := Usuario nombre: 'jose' contraseña: '1234'.
	usuario2 := Usuario nombre: 'pepe' contraseña: '1111'.
	post := Post cuerpo: 'A' usuario: usuario1.
	post darLike: usuario1.
	post darDislike: usuario2
]

{ #category : #test }
PostTest >> testCalcularPuntajePost [
	self assert: post calcularPuntajePost equals: 0.
	post darDislike: usuario1.
	self assert: post calcularPuntajePost equals: -2.
	post darLike: usuario1.
	post darLike: usuario2.
		self assert: post calcularPuntajePost equals: 2.
	
]

{ #category : #test }
PostTest >> testDarDislike [
	self assert: (post buscarDislike: usuario1) equals: false.
	self assert: (post buscarDislike: usuario2) equals: true.
	post darDislike: usuario1.
	self assert: (post buscarLike: usuario1) equals: false.
	self assert: (post buscarDislike: usuario1) equals: true
]

{ #category : #test }
PostTest >> testDarLike [
	self assert: (post buscarLike: usuario1) equals: true.
	self assert: (post buscarLike: usuario2) equals: false.
	post darLike: usuario2.
	self assert: (post buscarLike: usuario2) equals: true.
	self assert: (post buscarDislike: usuario2) equals: false
]
