"
Una Pregunta
"
Class {
	#name : #Pregunta,
	#superclass : #Post,
	#instVars : [
		'titulo',
		'topicos',
		'respuestas'
	],
	#category : #'CuOOra-model'
}

{ #category : #constructor }
Pregunta class >> cuerpo: unTexto usuario: unUsuario titulo: unTitulo topicos: unosTopicos [
	^ (self cuerpo: unTexto usuario: unUsuario)
		inicializarTitulo: unTitulo
		topicos: unosTopicos
]

{ #category : #methods }
Pregunta >> agregarRespuesta: unaRespuesta [
	respuestas add: unaRespuesta.
	unaRespuesta usuario agregarRespuesta: unaRespuesta
]

{ #category : #methods }
Pregunta >> borrarPregunta [
	usuario borrarPregunta: self.
	respuestas do: [ :r | r borrarRespuesta ].
	topicos do:[:t|t borrarPregunta:self]
]

{ #category : #initialization }
Pregunta >> inicializarTitulo: unTitulo topicos: unosTopicos [ 
	titulo := unTitulo.
	topicos := unosTopicos.
]

{ #category : #initialization }
Pregunta >> initialize [
	super initialize.
	titulo := DateAndTime now.
	topicos := OrderedCollection new.
	respuestas := OrderedCollection new.
]

{ #category : #accessing }
Pregunta >> obtenerRespuestasPregunta [
	^ respuestas sort: [:a :b | a calcularPuntajePost > b calcularPuntajePost ]
]

{ #category : #accessing }
Pregunta >> titulo [
	^ titulo
]

{ #category : #accessing }
Pregunta >> topicos [
	^ topicos
]
