"
Se muestra la información de la pregunta, se ofrece la posibilidad de que el usuario la vote, y se
listan todas sus respuestas.
Al hacer click en Agregar Respuesta debe dirigir a ResponderComponent.
"
Class {
	#name : #PreguntaComponent,
	#superclass : #WAComponent,
	#instVars : [
		'pregunta'
	],
	#category : #'CuOOra-ui'
}

{ #category : #callbacks }
PreguntaComponent >> agregarRespuesta [
  	self
		call:
			(ResponderComponent new
				pregunta:pregunta;
				yourself) 
  
]

{ #category : #initialization }
PreguntaComponent >> initialize [
	super initialize.

]

{ #category : #accessing }
PreguntaComponent >> pregunta [
	^ pregunta
]

{ #category : #accessing }
PreguntaComponent >> pregunta: anObject [
	pregunta := anObject
]

{ #category : #rendering }
PreguntaComponent >> renderAgregarRespuesta: aCanvas [
	aCanvas anchor 
	style: 'position:absolute;left:45%';
		callback: [ self agregarRespuesta ];
		with: [aCanvas button:'+ Agregar Respuesta'].
]

{ #category : #rendering }
PreguntaComponent >> renderContentOn: aCanvas [
	self renderHeader: aCanvas.
	self renderPregunta: aCanvas.
		self renderAgregarRespuesta: aCanvas.
	self renderRespuestas: aCanvas
]

{ #category : #rendering }
PreguntaComponent >> renderHeader: aCanvas [
	aCanvas
		paragraph: [ aCanvas div
				style:
					'display:flex; justify-content:space-between; align-items:center; background-color:orange';
				with: [ aCanvas anchor
					      style:'color:black;text-decoration:none';
							callback:[self call: (HomeComponent new)];
					      with: [aCanvas heading: 'CuOOra'].
					aCanvas
						div: [ aCanvas anchor
							style:'text-decoration:none;color:black';
							callback:[self verPerfil:self session user];
							with:[aCanvas strong: self session user nombre].
							aCanvas space.
							aCanvas space.
							aCanvas anchor
								callback: [ self unlog ];
								with: 'Log out' ] ] ].
	aCanvas horizontalRule.
		aCanvas anchor
		callback: [ self answer ];
		with: [ aCanvas button: '<--' ]
]

{ #category : #rendering }
PreguntaComponent >> renderPregunta: aCanvas [
	aCanvas heading
		level: 2;
		with: pregunta titulo.
	aCanvas
		paragraph: [ pregunta topicos
				do: [ :t | 
					aCanvas anchor
						style: 'color:grey;float:left; margin-left:5px';
						with: '#'.
					aCanvas anchor
						style: 'color:gray; float:left; margin-right:5px';
						with: t nombre ] ].
	aCanvas space.
	aCanvas paragraph
		style: 'margin-left:5px';
		with: pregunta cuerpo.
	aCanvas div
		style: 'overflow:hidden';
		with: [ aCanvas paragraph
				style: 'float:left; margin-left:5px';
				with: 'Realizada por '.
			aCanvas anchor
				style: 'float:left; margin-left:5px';
				callback: [ self verPerfil: pregunta usuario ];
				with: [ aCanvas paragraph: pregunta usuario nombre ] ].
	aCanvas div
		style: 'overflow:hidden';
		with: [ aCanvas paragraph
				style: 'float:left; margin-left:5px';
				with: pregunta likes size.
			aCanvas paragraph
				style: 'float:left; margin-left:5px';
				with: 'likes |'.
			aCanvas paragraph
				style: 'float:left; margin-left:5px';
				with: pregunta dislikes size.
			aCanvas paragraph
				style: 'float:left; margin-left:5px';
				with: 'dislikes' ].
	aCanvas div
		style: 'overflow hidden';
		with: [ aCanvas anchor
				style: 'float:left; margin-left:5px';
				callback: [ pregunta darLike: self session user ];
				with: 'like'.
			aCanvas anchor
				style: 'color:blue;float:left; margin-left:5px';
				with: '|'.
			aCanvas anchor
				style: 'float:left; margin-left:5px';
				callback: [ pregunta darDislike: self session user ];
				with: 'dislike' ].
	aCanvas space.

	aCanvas horizontalRule.

]

{ #category : #rendering }
PreguntaComponent >> renderRespuestas: aCanvas [
	aCanvas
		unorderedList: [ pregunta obtenerRespuestasPregunta 
				do: [ :r | 
					aCanvas div
						style: 'overflow:hidden';
						with: [ aCanvas heading
								level: 3;
								style: 'float:left';
								with: 'Respuesta de '.
							aCanvas heading
								level: 3;
								style: 'float:left; margin-left:5px';
								with: [ aCanvas anchor
										callback: [ self verPerfil: r usuario ];
										with: r usuario nombre ] ].
					aCanvas paragraph
						style: 'margin-left:5px';
						with: r cuerpo.
					aCanvas div
						style: 'overflow:hidden';
						with: [ aCanvas paragraph
								style: 'float:left; margin-left:5px';
								with: r likes size.
							aCanvas paragraph
								style: 'float:left; margin-left:5px';
								with: 'likes |'.
							aCanvas paragraph
								style: 'float:left; margin-left:5px';
								with: r dislikes size.
							aCanvas paragraph
								style: 'float:left; margin-left:5px';
								with: 'dislikes' ].
					aCanvas div
						style: 'overflow hidden';
						with: [ aCanvas anchor
								style: 'float:left; margin-left:5px';
								callback: [ r darLike: self session user ];
								with: 'like'.
							aCanvas anchor
								style: 'color:blue;float:left; margin-left:5px';
								with: '|'.
							aCanvas anchor
								style: 'float:left; margin-left:5px';
								callback: [ r darDislike: self session user ];
								with: 'dislike' ].
					aCanvas space.
					aCanvas horizontalRule ] ]
]

{ #category : #callbacks }
PreguntaComponent >> unlog [
self session unregister.
self requestContext redirectTo: self application url. 
]

{ #category : #callbacks }
PreguntaComponent >> verPerfil: unUsuario [
	self
		call:
			(PerfilComponent
				new usuario: unUsuario ;
				yourself)
]
