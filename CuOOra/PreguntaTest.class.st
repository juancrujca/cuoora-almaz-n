Class {
	#name : #PreguntaTest,
	#superclass : #TestCase,
	#instVars : [
		'pregunta',
		'respuesta1',
		'respuesta2',
		'usuario',
		'topicos'
	],
	#category : #'CuOOra-tests'
}

{ #category : #setup }
PreguntaTest >> setUp [
	super setUp.
	usuario := Usuario nombre: 'jose' contraseña: '1234'.
	topicos := OrderedCollection new.
	pregunta := Pregunta
		cuerpo: '1'
		usuario: usuario
		titulo: 'A'
		topicos: topicos.
	respuesta1 := Respuesta
		cuerpo: '1'
		usuario: usuario
		pregunta: pregunta.
	respuesta2 := Respuesta
		cuerpo: '2'
		usuario: usuario
		pregunta: pregunta.
	usuario agregarPregunta: pregunta.
	pregunta agregarRespuesta: respuesta1.
	pregunta agregarRespuesta: respuesta2
]

{ #category : #test }
PreguntaTest >> testAgregarRespuesta [
	self assert: pregunta obtenerRespuestasPregunta size equals: 2.
]

{ #category : #test }
PreguntaTest >> testBorrarPregunta [
	pregunta borrarPregunta.
	self assert: (usuario obtenerPreguntasUsuario includes: pregunta) equals:false.
]
