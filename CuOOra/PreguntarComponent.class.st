"
Cuando el usuario hace click en “Hacer una nueva pregunta”, se debe mostrar un formulario de creación, donde el usuario puede escribir el título de la pregunta, su desarrollo y los tópicos. Los tópicos se
ingresan separados por coma.
"
Class {
	#name : #PreguntarComponent,
	#superclass : #WAComponent,
	#instVars : [
		'titulo',
		'cuerpo',
		'topicos',
		'sistema'
	],
	#category : #'CuOOra-ui'
}

{ #category : #callbacks }
PreguntarComponent >> cancelar [
	self answer
]

{ #category : #accessing }
PreguntarComponent >> cuerpo [
	^ cuerpo
]

{ #category : #accessing }
PreguntarComponent >> cuerpo: anObject [
	cuerpo := anObject
]

{ #category : #callbacks }
PreguntarComponent >> guardar [
	self session user
		agregarPregunta:
			(Pregunta
				cuerpo: cuerpo
				usuario: self session user
				titulo: titulo
				topicos: topicos).
	self call: (HomeComponent new)
]

{ #category : #initialization }
PreguntarComponent >> initialize [
	super initialize.
	topicos := OrderedCollection new.
	sistema := Sistema soleInstance
]

{ #category : #rendering }
PreguntarComponent >> renderContentOn: aCanvas [ 

	self renderHeader: aCanvas .
	self renderPreguntar: aCanvas .
]

{ #category : #rendering }
PreguntarComponent >> renderForm: aCanvas [
	aCanvas
		form: [ aCanvas paragraph: 'Título:'.
			aCanvas paragraph: [ aCanvas textInput on: #titulo of: self ].
			aCanvas paragraph: 'Desarrollo:'.
			aCanvas
				paragraph: [ aCanvas textArea
						style: 'resize: none';
						columns: 50;
						rows: 7;
						on: #cuerpo of: self ].
					aCanvas paragraph: 'Tópicos:'.
			aCanvas
				paragraph: [ 
					self renderTopicos: aCanvas ].
			aCanvas break.
			aCanvas button
				callback: [ self cancelar ];
				with: 'Cancelar'.
			aCanvas button
				callback: [ self guardar ];
				with: 'Guardar' ]
]

{ #category : #rendering }
PreguntarComponent >> renderHeader: aCanvas [
	aCanvas
		paragraph: [ aCanvas div
				style:
					'display:flex; justify-content:space-between; align-items:center; background-color:orange';
				with: [ aCanvas anchor
					      style:'color:black;text-decoration:none';
							callback:[self call: (HomeComponent new)];
					      with: [aCanvas heading: 'CuOOra'].
					aCanvas
						div: [ aCanvas anchor
							style:'text-decoration:none;color:black';
							callback:[self verPerfil:self session user];
							with:[aCanvas strong: self session user nombre].
							aCanvas space.
							aCanvas space.
							aCanvas anchor
								callback: [ self unlog ];
								with: 'Log out' ] ] ].
	aCanvas horizontalRule.
	aCanvas anchor
		callback: [ self answer ];
		with: [ aCanvas button: '<--' ]
]

{ #category : #rendering }
PreguntarComponent >> renderPreguntar: aCanvas [
	aCanvas heading: 'Nueva Pregunta'.
	aCanvas horizontalRule.
	self renderForm: aCanvas.
]

{ #category : #rendering }
PreguntarComponent >> renderTopicos: aCanvas [
	sistema topicos
		do: [ :t | 
			aCanvas checkbox value: false;
			onTrue: [ topicos add: t ]
                             onFalse: [  ].
			aCanvas text: t nombre ]
]

{ #category : #accessing }
PreguntarComponent >> sistema [
	^ sistema
]

{ #category : #accessing }
PreguntarComponent >> sistema: anObject [
	sistema := anObject
]

{ #category : #accessing }
PreguntarComponent >> titulo [
	^ titulo
]

{ #category : #accessing }
PreguntarComponent >> titulo: anObject [
	titulo := anObject
]

{ #category : #accessing }
PreguntarComponent >> topicos [
	^ topicos
]

{ #category : #accessing }
PreguntarComponent >> topicos: anObject [
	topicos := anObject
]

{ #category : #callbacks }
PreguntarComponent >> unlog [
	self session unregister.
	self requestContext redirectTo: self application url
]

{ #category : #callbacks }
PreguntarComponent >> verPerfil: unUsuario [
	self
		call:
			(PerfilComponent
				new usuario: unUsuario ;
				yourself)
]
