"
Se muestra la Pregunta y abajo un text area para ingresar la respuesta, se puede guardar o cancelar la misma.
"
Class {
	#name : #ResponderComponent,
	#superclass : #WAComponent,
	#instVars : [
		'cuerpo',
		'pregunta'
	],
	#category : #'CuOOra-ui'
}

{ #category : #callbacks }
ResponderComponent >> cancelar [
	self answer
]

{ #category : #accessing }
ResponderComponent >> cuerpo [
	^ cuerpo
]

{ #category : #accessing }
ResponderComponent >> cuerpo: anObject [
	cuerpo := anObject
]

{ #category : #callbacks }
ResponderComponent >> guardar [
	self pregunta
		agregarRespuesta:
			(Respuesta
				cuerpo: self cuerpo
				usuario: self session user
				pregunta: self pregunta).
	self
		call:
			(PreguntaComponent new
				pregunta: self pregunta;
				yourself)
]

{ #category : #accessing }
ResponderComponent >> pregunta [
	^ pregunta
]

{ #category : #accessing }
ResponderComponent >> pregunta: anObject [
	pregunta := anObject
]

{ #category : #rendering }
ResponderComponent >> renderContentOn: aCanvas [
	self renderHeader:aCanvas.
	self renderPregunta:aCanvas.
	self renderResponder:aCanvas.
]

{ #category : #rendering }
ResponderComponent >> renderHeader: aCanvas [
	aCanvas
		paragraph: [ aCanvas div
				style:
					'display:flex; justify-content:space-between; align-items:center; background-color:orange';
				with: [ aCanvas anchor
					      style:'color:black;text-decoration:none';
							callback:[self call: (HomeComponent new)];
					      with: [aCanvas heading: 'CuOOra'].
					aCanvas
						div: [ aCanvas anchor
							style:'text-decoration:none;color:black';
							callback:[self verPerfil:self session user];
							with:[aCanvas strong: self session user nombre].
							aCanvas space.
							aCanvas space.
							aCanvas anchor
								callback: [ self unlog ];
								with: 'Log out' ] ] ].
	aCanvas horizontalRule.
		aCanvas anchor
		callback: [ self answer ];
		with: [ aCanvas button: '<--' ]
]

{ #category : #rendering }
ResponderComponent >> renderPregunta: aCanvas [
	aCanvas heading
		level: 2;
		with: pregunta titulo.
	aCanvas
		paragraph: [ pregunta topicos
				do: [ :t | 
					aCanvas anchor
						style: 'color:grey;float:left; margin-left:5px';
						with: '#'.
					aCanvas anchor
						style: 'color:gray; float:left; margin-right:5px';
						with: t nombre ] ].
	aCanvas space.
	aCanvas div
		style: 'overflow:hidden';
		with: [ aCanvas paragraph
				style: 'float:left; margin-left:5px';
				with: 'Realizada por '.
			aCanvas anchor
				style: 'float:left; margin-left:5px';
				callback: [ self verPerfil: pregunta usuario ];
				with: [ aCanvas paragraph: pregunta usuario nombre ] ].
	aCanvas div
		style: 'overflow:hidden';
		with: [ aCanvas paragraph
				style: 'float:left; margin-left:5px';
				with: pregunta likes size.
			aCanvas paragraph
				style: 'float:left; margin-left:5px';
				with: 'likes |'.
			aCanvas paragraph
				style: 'float:left; margin-left:5px';
				with: pregunta dislikes size.
			aCanvas paragraph
				style: 'float:left; margin-left:5px';
				with: 'dislikes' ].
	aCanvas div
		style: 'overflow hidden';
		with: [ aCanvas anchor
				style: 'float:left; margin-left:5px';
				callback: [ pregunta darLike: self session user ];
				with: 'like'.
			aCanvas anchor
				style: 'color:blue;float:left; margin-left:5px';
				with: '|'.
			aCanvas anchor
				style: 'float:left; margin-left:5px';
				callback: [ pregunta darDislike: self session user ];
				with: 'dislike' ].
	aCanvas space.
	aCanvas horizontalRule
]

{ #category : #rendering }
ResponderComponent >> renderResponder: aCanvas [
	aCanvas heading
		style: 'text-align:center';
		level: 3;
		with: 'RESPONDER:'.
	aCanvas form
		style: 'text-align:center';
		with: [
			aCanvas textArea
				style: 'resize: none';
				columns: 50;
				rows: 7;
				on: #cuerpo of: self.
			aCanvas break.
			aCanvas button
				callback: [ self cancelar ];
				with: 'Cancelar'.
			aCanvas button
				callback: [ self guardar ];
				with: 'Guardar' ]
]

{ #category : #callbacks }
ResponderComponent >> unlog [
	self session unregister.
	self requestContext redirectTo: self application url
]

{ #category : #callbacks }
ResponderComponent >> verPerfil: unUsuario [
	self
		call:
			(PerfilComponent new
				usuario: unUsuario;
				yourself)
]
