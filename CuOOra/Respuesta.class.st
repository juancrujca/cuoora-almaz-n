"
Una Respuesta
"
Class {
	#name : #Respuesta,
	#superclass : #Post,
	#instVars : [
		'pregunta'
	],
	#category : #'CuOOra-model'
}

{ #category : #constructor }
Respuesta class >> cuerpo: unTexto usuario: unUsuario pregunta: unaPregunta [
	^ (self cuerpo: unTexto usuario: unUsuario) inicializarPregunta: unaPregunta 
]

{ #category : #methods }
Respuesta >> borrarRespuesta [
  usuario borrarRespuesta:self.
]

{ #category : #initialization }
Respuesta >> inicializarPregunta: unaPregunta [
	pregunta := unaPregunta.
]

{ #category : #accessing }
Respuesta >> pregunta [
	^ pregunta
]
