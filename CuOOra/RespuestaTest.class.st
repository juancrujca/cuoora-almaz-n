Class {
	#name : #RespuestaTest,
	#superclass : #TestCase,
	#instVars : [
		'usuario',
		'pregunta',
		'respuesta1',
		'respuesta2',
		'topicos'
	],
	#category : #'CuOOra-tests'
}

{ #category : #setup }
RespuestaTest >> setUp [
	super setUp.
	usuario := Usuario nombre: 'jose' contraseña: '1234'.
	topicos := OrderedCollection new.
	pregunta := Pregunta
		cuerpo: '1'
		usuario: usuario
		titulo: 'A'
		topicos: topicos.
	respuesta1 := Respuesta
		cuerpo: '1'
		usuario: usuario
		pregunta: pregunta.
	respuesta2 := Respuesta
		cuerpo: '2'
		usuario: usuario
		pregunta: pregunta.
	pregunta agregarRespuesta: respuesta1.
		pregunta agregarRespuesta: respuesta2.
]

{ #category : #test }
RespuestaTest >> testBorrarRespuesta [
	self assert: usuario obtenerRespuestasUsuario size equals: 2.
	respuesta1 borrarRespuesta.
	self assert: usuario obtenerRespuestasUsuario size equals: 1.
		respuesta2 borrarRespuesta.
	self assert: usuario obtenerRespuestasUsuario size equals: 0.
]
