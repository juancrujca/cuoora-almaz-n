"
Sistema de CuOOra
"
Class {
	#name : #Sistema,
	#superclass : #Object,
	#instVars : [
		'usuarios',
		'topicos'
	],
	#classInstVars : [
		'soleInstance'
	],
	#category : #'CuOOra-model'
}

{ #category : #'instance creation' }
Sistema class >> clearSoleInstance [
	soleInstance :=nil.
]

{ #category : #'instance creation' }
Sistema class >> soleInstance [
	^soleInstance ifNil: [ soleInstance := self new ].
	
]

{ #category : #methods }
Sistema >> agregarTopico: unTopico [
	topicos add: unTopico
]

{ #category : #methods }
Sistema >> crearUsuario: unUsuario [
	usuarios add: unUsuario
]

{ #category : #initialization }
Sistema >> initialize [

	super initialize.
	usuarios := OrderedCollection new.
	topicos := OrderedCollection new.

]

{ #category : #accessing }
Sistema >> topicos [
	^ topicos
]

{ #category : #methods }
Sistema >> userWithNombre: unNombre protectedBy: unaContraseña [	^ usuarios		detect: [ :each | each nombre = unNombre & (each contraseña = unaContraseña) ] 		ifNone: [ nil ]
]

{ #category : #accessing }
Sistema >> usuarios [
	^ usuarios
]
