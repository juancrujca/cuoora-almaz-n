Class {
	#name : #SistemaTest,
	#superclass : #TestCase,
	#instVars : [
		'sistema',
		'usuario1',
		'usuario2',
		'topico1',
		'topico2'
	],
	#category : #'CuOOra-tests'
}

{ #category : #setup }
SistemaTest >> setUp [
	sistema := Sistema new.
	
	usuario1 := Usuario nombre: 'pepe' contraseña: '1234'.
		sistema crearUsuario: usuario1.
]

{ #category : #test }
SistemaTest >> testAgregarTopico [
	"el sistema tiene precargado 3 topicos para el ui"

	self assert: sistema topicos size equals: 3.
	topico1 := Topico nombre: 'OO1' descripcion: 'la materia'.
	topico2 := Topico nombre: 'Smalltalk' descripcion: 'el leguaje'.
	sistema agregarTopico: topico1.
	self assert: sistema topicos size equals: 4.
	sistema agregarTopico: topico2.
	self assert: sistema topicos size equals: 5
]

{ #category : #test }
SistemaTest >> testCrearUsuario [
	"el sistema tiene precargado 3 usuarios para el ui"

	self assert: sistema usuarios size equals: 4.
	usuario1 := Usuario nombre: 'jose' contraseña: '1111'.

	sistema crearUsuario: usuario2.
	self assert: sistema usuarios size equals: 5
]

{ #category : #test }
SistemaTest >> testUserWithNombreProtectedBy [
	self
		assert:
			(sistema
				userWithNombre: 'pepe'
				protectedBy: '1234') isNotNil
		equals: true
]
