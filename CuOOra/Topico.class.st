"
Un Topico de CuOOra
"
Class {
	#name : #Topico,
	#superclass : #Object,
	#instVars : [
		'nombre',
		'descripcion',
		'preguntas'
	],
	#category : #'CuOOra-model'
}

{ #category : #constructor }
Topico class >> nombre: unNombre descripcion: unaDescripcion [
	^ self new inicializarNombre: unNombre descripcion: unaDescripcion
]

{ #category : #methods }
Topico >> agregarPregunta: unaPregunta [ 
	preguntas
		add: unaPregunta 
]

{ #category : #methods }
Topico >> borrarPregunta: unaPregunta [ 
 preguntas remove: unaPregunta 
]

{ #category : #accessing }
Topico >> descripcion [
	^ descripcion
]

{ #category : #initialization }
Topico >> inicializarNombre: unNombre descripcion: unaDescripcion [
	nombre := unNombre.
	descripcion  := unaDescripcion 
]

{ #category : #initialization }
Topico >> initialize [
	super initialize.
	preguntas := OrderedCollection new
]

{ #category : #accessing }
Topico >> nombre [
	^ nombre
]

{ #category : #accessing }
Topico >> obtenerPreguntasTopico [ 
	^ preguntas
]
