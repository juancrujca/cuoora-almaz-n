Class {
	#name : #TopicoTest,
	#superclass : #TestCase,
	#instVars : [
		'topico',
		'pregunta1',
		'pregunta2'
	],
	#category : #'CuOOra-tests'
}

{ #category : #setup }
TopicoTest >> setUp [
	|usuario topicos|
	topico := Topico nombre: 'A' descripcion: '1'.
	usuario := Usuario nombre: 'jose' contraseña: '1234'.
	topicos := OrderedCollection new.
	topicos add: topico.
	pregunta1 := Pregunta
		cuerpo: '1'
		usuario: usuario
		titulo: 'A'
		topicos: topicos.
	topico agregarPregunta: pregunta1.
	pregunta2 := Pregunta
		cuerpo: '2'
		usuario: usuario
		titulo: 'B'
		topicos: topicos.
	topico agregarPregunta: pregunta2
]

{ #category : #test }
TopicoTest >> testAgregarPregunta [
	self assert: topico obtenerPreguntasTopico  size equals: 2.
	self assert: (topico obtenerPreguntasTopico  at: 1) titulo equals: 'A'.
	self assert: (topico obtenerPreguntasTopico  at: 2) titulo equals: 'B'
]

{ #category : #test }
TopicoTest >> testBorrarPregunta [
	topico borrarPregunta: pregunta1.
	self assert: topico obtenerPreguntasTopico  size equals: 1.
	topico borrarPregunta: pregunta2.
	self assert: topico obtenerPreguntasTopico  size equals: 0
]
