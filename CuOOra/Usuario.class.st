"
Un usuario de CuOOra
"
Class {
	#name : #Usuario,
	#superclass : #Object,
	#instVars : [
		'nombre',
		'contraseña',
		'creacion',
		'seguidos',
		'favoritos',
		'preguntas',
		'respuestas'
	],
	#category : #'CuOOra-model'
}

{ #category : #constructor }
Usuario class >> nombre: unNombre contraseña: unaContraseña [
	^ self new inicializarNombre: unNombre contraseña: unaContraseña .
]

{ #category : #methods }
Usuario >> agregarFavorito: unTopico [
  favoritos add:unTopico.
]

{ #category : #methods }
Usuario >> agregarPregunta: unaPregunta [
	preguntas add: unaPregunta.
	unaPregunta topicos
		do:
			[ :t | t agregarPregunta: unaPregunta ]
]

{ #category : #methods }
Usuario >> agregarRespuesta: unaRespuesta [ 
	respuestas add: unaRespuesta 
]

{ #category : #methods }
Usuario >> borrarPregunta: unaPregunta [
	preguntas remove: unaPregunta.
]

{ #category : #methods }
Usuario >> borrarRespuesta: unaRespuesta [ 
	respuestas remove: unaRespuesta 
]

{ #category : #methods }
Usuario >> calcularPuntajeUsuario [

	| puntaje preguntasUsuario respuestasUsuario |
	preguntasUsuario := self obtenerPreguntasUsuario.
	respuestasUsuario := self obtenerRespuestasUsuario.
	puntaje := preguntasUsuario size * 20 + (respuestasUsuario size * 50).
	puntaje := puntaje
		+ (preguntasUsuario sumNumbers: [ :each | each calcularPuntajePost ]).
	puntaje := puntaje
		+ (respuestasUsuario sumNumbers: [ :each | each calcularPuntajePost ]).
	^ puntaje
]

{ #category : #accessing }
Usuario >> contraseña [
	^ contraseña
]

{ #category : #accessing }
Usuario >> favoritos [
	^ favoritos
]

{ #category : #initialization }
Usuario >> inicializarNombre: unNombre contraseña: unaContraseña [
	nombre := unNombre .
	contraseña := unaContraseña .
]

{ #category : #initialization }
Usuario >> initialize [
	super initialize.
	creacion := DateAndTime now.
	seguidos := OrderedCollection new.
	favoritos := OrderedCollection new.
	preguntas := OrderedCollection new.
	respuestas := OrderedCollection new.
]

{ #category : #accessing }
Usuario >> nombre [
	^ nombre
]

{ #category : #accessing }
Usuario >> obtenerPreguntasRelevantes [
	^ (favoritos flatCollect: [ :f | f obtenerPreguntasTopico ])
		union: (seguidos flatCollect: [ :s | s obtenerPreguntasUsuario ])
]

{ #category : #accessing }
Usuario >> obtenerPreguntasUsuario [ 
	^preguntas 
]

{ #category : #accessing }
Usuario >> obtenerRespuestasUsuario [
	^ respuestas 
]

{ #category : #accessing }
Usuario >> seguidos [
	^seguidos
]

{ #category : #methods }
Usuario >> seguirA: unUsuario [
	seguidos add: unUsuario 
]
