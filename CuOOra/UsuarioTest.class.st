Class {
	#name : #UsuarioTest,
	#superclass : #TestCase,
	#instVars : [
		'usuario',
		'usuario2',
		'favorito',
		'pregunta1',
		'pregunta2',
		'respuesta1',
		'respuesta2'
	],
	#category : #'CuOOra-tests'
}

{ #category : #setup }
UsuarioTest >> setUp [
	super setUp.
	usuario := Usuario nombre: 'jose' contraseña: '1234'.
	favorito := Topico nombre: 'A' descripcion: '1'.
	usuario agregarFavorito: favorito.
	favorito := Topico nombre: 'B' descripcion: '2'.
	usuario agregarFavorito: favorito.
	pregunta1 := Pregunta
		cuerpo: '1'
		usuario: self
		titulo: 'A'
		topicos: usuario favoritos.
		pregunta1 darDislike: usuario.
	usuario agregarPregunta: pregunta1.
	pregunta2 := Pregunta
		cuerpo: '2'
		usuario: self
		titulo: 'B'
		topicos: usuario favoritos.
	respuesta1 := Respuesta
		cuerpo: 'A'
		usuario: usuario
		pregunta: pregunta2.
	pregunta2 agregarRespuesta: respuesta1.
	respuesta2 := Respuesta
		cuerpo: 'B'
		usuario: usuario
		pregunta: pregunta2.
	pregunta2 agregarRespuesta: respuesta2.
	usuario agregarPregunta: pregunta2.
	usuario2 := Usuario nombre: 'pedro' contraseña: '1111'.
	usuario seguirA: usuario2
]

{ #category : #test }
UsuarioTest >> testAgregarFavorito [
	self assert: usuario favoritos size equals: 2.
	self assert: (usuario favoritos at: 1) = favorito equals: false.
	self assert: (usuario favoritos at: 2) = favorito equals: true
]

{ #category : #test }
UsuarioTest >> testAgregarPregunta [
	self assert: usuario obtenerPreguntasUsuario size equals: 2.
	self assert: (usuario obtenerPreguntasUsuario at: 1) titulo equals: 'A'.
	self assert: (usuario obtenerPreguntasUsuario at: 2) titulo equals: 'B'
]

{ #category : #test }
UsuarioTest >> testAgregarRespuesta [
	self assert: usuario obtenerRespuestasUsuario size equals: 2.
	self
		assert:
			(usuario obtenerRespuestasUsuario at: 1)
				= (usuario obtenerRespuestasUsuario at: 2)
		equals: false
]

{ #category : #test }
UsuarioTest >> testBorrarPregunta [
	usuario borrarPregunta: pregunta1.
	self assert: usuario obtenerPreguntasUsuario size equals: 1.
	usuario borrarPregunta: pregunta2.
	self assert: usuario obtenerPreguntasUsuario size equals: 0
]

{ #category : #test }
UsuarioTest >> testBorrarRespuesta [
	usuario borrarRespuesta: respuesta1.
	self assert: usuario obtenerRespuestasUsuario size equals: 1
]

{ #category : #test }
UsuarioTest >> testCalcularPuntajeUsuario [
	self assert: usuario calcularPuntajeUsuario equals: 139.
]

{ #category : #test }
UsuarioTest >> testSeguirA [
	self assert: usuario seguidos size equals: 1.
	self assert: (usuario seguidos at: 1) nombre equals: 'pedro'.
		self assert: (usuario seguidos at: 1) contraseña equals: '1111'
]
