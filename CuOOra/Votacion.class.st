"
Un Voto
"
Class {
	#name : #Votacion,
	#superclass : #Object,
	#instVars : [
		'creacion',
		'votante'
	],
	#category : #'CuOOra-model'
}

{ #category : #constructor }
Votacion class >> votante: unVotante [
	^ self new inicializarVotante: unVotante 
]

{ #category : #methods }
Votacion >> compararVotante: unVotante [
	^ votante = unVotante
]

{ #category : #initialization }
Votacion >> inicializarVotante: unVotante [

	votante := unVotante 
]

{ #category : #initialization }
Votacion >> initialize [
super initialize .
	creacion := DateAndTime now.
]
