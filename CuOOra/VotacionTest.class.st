Class {
	#name : #VotacionTest,
	#superclass : #TestCase,
	#instVars : [
		'votacion',
		'votante'
	],
	#category : #'CuOOra-tests'
}

{ #category : #setup }
VotacionTest >> setUp [
	super setUp.
	votacion := Votacion new.
	votante := Usuario nombre: 'A' contraseña: '1'.
	votacion inicializarVotante: votante
]

{ #category : #test }
VotacionTest >> testCompararVotante [
	| unVotante  |
	unVotante  := Usuario nombre: 'A' contraseña: '1'.
	self assert: (votacion compararVotante: votante) equals: true.
	self assert: (votacion compararVotante: unVotante ) equals: false
]
